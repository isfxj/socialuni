package com.socialuni.social.sdk.dao.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TagMapper {
    Integer TagCount();
}
