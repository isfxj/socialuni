package com.socialuni.social.sdk.dao.repository.community;

import com.socialuni.social.sdk.dao.DO.community.talk.SocialTalkTagDO;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * TODO〈一句话功能简述〉
 * TODO〈功能详细描述〉
 *
 * @author qinkaiyuan
 * @since TODO[起始版本号]
 */
public interface TalkTagRepository extends JpaRepository<SocialTalkTagDO, Integer> {


}