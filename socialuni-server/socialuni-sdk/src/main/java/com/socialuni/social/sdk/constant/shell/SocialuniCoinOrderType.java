package com.socialuni.social.sdk.constant.shell;

/**
 * 金币订单类型
 *
 * @author qinkaiyuan
 * @date 2018-09-16 10:58
 */
public class SocialuniCoinOrderType {
    //充值
    public static final String recharge = "充值";
    //consume
    public static final String expense = "消费";
    public static final String income = "收入";
}

