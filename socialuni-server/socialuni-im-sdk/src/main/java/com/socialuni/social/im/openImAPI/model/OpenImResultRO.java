package com.socialuni.social.im.openImAPI.model;

import lombok.Data;

@Data
public class OpenImResultRO {
    Integer errCode;
    String errMsg;
}