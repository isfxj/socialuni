package com.socialuni.social.user.sdk.model.RO;

import lombok.Data;

@Data
public class QQMapGeocoderRO {
    QQMapGeocoderResultRO result;
}
